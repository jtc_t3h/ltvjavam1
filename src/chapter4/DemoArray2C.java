package chapter4;

public class DemoArray2C {

	public static void main(String[] args) {

		// declare = value
		int arr1[][] = new int[3][4];
		int [][]arr2 = new int[][]{{1,2,3},{4,5,6}};
		int []arr[] = {{1,2,3},{4,5,6}};
		
		System.out.println("rowCount = " + arr.length);
		System.out.println("columnCount " + arr[0].length);
		System.out.println("arr[1][1] =" + arr[1][1]); // 5
		
		// for-each
		for (int[] row: arr) {
			for (int element: row) {
				System.out.println(element);
			}
		}
		
		DemoArray2C demo = new DemoArray2C();
		demo.printArray2C(arr1);
	}
	
	// khai bao ham
	public void printArray2C(int []arr[]) {
		for (int[] row: arr) {
			for (int element: row) {
				System.out.println(element);
			}
		}
	}

}
