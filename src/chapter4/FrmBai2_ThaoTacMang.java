package chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;

public class FrmBai2_ThaoTacMang extends JFrame {

	private JPanel contentPane;
	private JTextField txtN;
	private JTextField txtX;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_ThaoTacMang frame = new FrmBai2_ThaoTacMang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_ThaoTacMang() {
		setTitle("Thực hiện các thao tác xử lý trên mảng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 534, 451);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nhập n:");
		lblNewLabel.setBounds(10, 11, 83, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNhpX = new JLabel("Nhập x:");
		lblNhpX.setBounds(10, 36, 83, 14);
		contentPane.add(lblNhpX);
		
		JLabel lblMngcPht = new JLabel("Mảng được phát sinh");
		lblMngcPht.setBounds(10, 61, 143, 14);
		contentPane.add(lblMngcPht);
		
		JLabel lblKtQuTm = new JLabel("Kết quả tìm kiếm");
		lblKtQuTm.setBounds(10, 223, 143, 14);
		contentPane.add(lblKtQuTm);
		
		txtN = new JTextField();
		txtN.setBounds(164, 8, 120, 20);
		contentPane.add(txtN);
		txtN.setColumns(10);
		
		txtX = new JTextField();
		txtX.setColumns(10);
		txtX.setBounds(164, 33, 120, 20);
		contentPane.add(txtX);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(163, 61, 346, 148);
		contentPane.add(scrollPane);
		
		JTextArea taMang = new JTextArea();
		taMang.setLineWrap(true);
		taMang.setLocation(113, 0);
		scrollPane.setViewportView(taMang);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(161, 220, 346, 146);
		contentPane.add(scrollPane_1);
		
		JTextArea taKetQua = new JTextArea();
		taKetQua.setLineWrap(true);
		taKetQua.setLocation(0, 158);
		scrollPane_1.setViewportView(taKetQua);
		
		JButton btnTmKim = new JButton("Tìm kiếm");
		btnTmKim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// B1: Lấy dữ liệu người dùng nhập vào
				int n = Integer.parseInt(txtN.getText().trim());
				int x = Integer.parseInt(txtX.getText().trim());
				
				// B2: Sinh ngau nhien N so nguyen (>0)
				int[] arr = generate(n);
				
				// B3: Thực hiện thao trên màng 
				String[] kq	= search(arr, x);
				
				// B4: Hiển thị ra màn hình
				StringBuilder sbMang = new StringBuilder();
				for (int element: arr) {
					sbMang.append(element + " ");
				}
				taMang.setText(sbMang.toString().trim());
				
				StringBuilder sbKq = new StringBuilder();
				for (String element: kq) {
					sbKq.append(element + "\n");
				}
				taKetQua.setText(sbKq.toString().trim());
			}
		});
		btnTmKim.setBounds(161, 377, 120, 23);
		contentPane.add(btnTmKim);
	}

	protected String[] search(int[] arr, int x) {
		String[] arrKq = new String[3];

		// Tìm kiếm x có trong mảng hay không
		int idxSearch = -1;
		for (int idx = 0; idx < arr.length; idx++) {
			if (arr[idx] == x) {
				idxSearch = idx;
				break;
			}
		}
		
		boolean isGreater = true;
		StringBuilder strGreater = new StringBuilder();
		for (int element: arr) {
			if (x < element) {
				isGreater = false;
				strGreater.append(element + " ");
			}
		}
		
		if (idxSearch > -1) {
			arrKq[0] = "Tìm thấy tại vị trí " + idxSearch;
		} else {
			arrKq[0] = "Không tìm thấy x.";
		}
		if (isGreater) {
			arrKq[1] = "X lớn hơn tất cả các phần tử trong mảng.";
		} else {
			arrKq[1] = "X KHÔNG lớn hơn tất cả các phần tử trong mảng.";
			arrKq[2] = "X nhỏ hơn các phần tử: " + strGreater.toString().trim();
		}
		
		return arrKq;
	}

	/**
	 * Sinh ra ngẫu nhiên N số nguyên lơn hơn 0
	 * @param n: số lượng phần tử của mảng
	 * 
	 * @return: 
	 * 		int[]: mảng số nguyên
	 */
	protected int[] generate(int n) {
		
		// Để sinh ra số ngẫu nhiên thì có 2 phương pháp:
		// Cách 1: dùng lớp java.util.Random 
		Random rd = new Random();
		
		int arr[] = new int[n];
		for (int idx = 0; idx < n; idx++) {
			arr[idx] = rd.nextInt(1000);
		}
		
		// Cách 2: dùng hàm random() của lớp java.lang.Math
		
		
		return arr;
	}
	
	
}
