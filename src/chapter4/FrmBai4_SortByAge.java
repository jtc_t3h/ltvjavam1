package chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Comparator;
import java.awt.event.ActionEvent;

public class FrmBai4_SortByAge extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBirth;

	private String[] arr = new String[100];
	private int currentSize = 0;
	private JList lstMain;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_SortByAge frame = new FrmBai4_SortByAge();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_SortByAge() {
		setTitle("Sort By Age");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 329, 340);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(6, 11, 61, 16);
		contentPane.add(lblName);
		
		JLabel lblNewLabel = new JLabel("Birth");
		lblNewLabel.setBounds(6, 43, 61, 16);
		contentPane.add(lblNewLabel);
		
		txtName = new JTextField();
		txtName.setBounds(69, 6, 130, 26);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtBirth = new JTextField();
		txtBirth.setColumns(10);
		txtBirth.setBounds(69, 38, 130, 26);
		contentPane.add(txtBirth);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// B1. Lấy thông tin Tên và năm sinh
				String name = txtName.getText().trim();
				String birth = txtBirth.getText().trim();
				
				// B2. Thêm thông tin vào mảng arr
				arr[currentSize++] = name + " " + birth;
				
				// B3. sắp xếp lại arr theo năm sinh tăng (tuổi giảm) 
				// => tạo ra mảng tạm loại bỏ những phần tử null trước khi sắp xếp
				String [] tempArr = new String[currentSize];
				for (int idx = 0; idx < currentSize; idx++) {
					tempArr[idx] = arr[idx];
				}
				Arrays.sort(tempArr, new Comparator<String>() {

					@Override
					public int compare(String o1, String o2) {
						String[] split1 = o1.split(" ");
						String[] split2 = o2.split(" ");
						
						return Integer.parseInt(split1[1]) - Integer.parseInt(split2[1]);
					}
				});
				
				// B4. Hiện thị danh sách ra lstMain
				DefaultListModel<String> model = (DefaultListModel<String>) lstMain.getModel();
				model.removeAllElements();
				for(String element: tempArr) {
					model.addElement(element);
				}
				lstMain.setModel(model);
			}
		});
		btnAdd.setBounds(208, 6, 104, 53);
		contentPane.add(btnAdd);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 71, 317, 241);
		contentPane.add(scrollPane);
		
		lstMain = new JList();
		scrollPane.setViewportView(lstMain);
		lstMain.setModel(new DefaultListModel<String>());
	}
}
