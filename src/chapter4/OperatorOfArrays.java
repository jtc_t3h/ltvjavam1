package chapter4;

import java.util.Arrays;
import java.util.Comparator;

public class OperatorOfArrays {

	public static void main(String[] args) {
		String[] arr = {"Loan 2", "An 8", "Nguyen 10", "Phuc 2"};
		
		// Sap xep theo abc -> dung java.utils.Arrays
		Arrays.sort(arr);
		
		for (String element: arr) {
			System.out.println(element);
		}
		
		// Sap xep array theo tieu chi (khac abc) -> dung java.util.Arrays
		Arrays.sort(arr, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				String[] split1 = o1.split(" ");
				String[] split2 = o2.split(" ");
				
				return Integer.parseInt(split1[1]) - Integer.parseInt(split2[1]);
			}
		});
		for (String element: arr) {
			System.out.println(element);
		}
		
	}
}
