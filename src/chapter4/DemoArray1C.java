package chapter4;

import java.util.Arrays;

public class DemoArray1C {

	public static void main(String[] args) {
		
		// Khai bao va khoi tao gia tri declar = value
		
		// Cách 1
		int[] arr1 = new int[5];
		int arr2[] = new int[5];
		
		// Cách 2
		Integer [] arr3 = {1,4,5,2,3};
		
		int arr4 [] = new int[] {6,7,8,9,10};
			
		// for (init; condition; step)
		for (int idx = 0; idx < arr3.length; idx++) {
			System.out.println("Element " + arr3[idx]);
		}
		
		// for-each -> cú pháp: for (DataType varName: Collection){}
		for (int element: arr4) {
			System.out.println(element);
		}
		
		Arrays.sort(arr3);
	
		System.out.println("Sap xep tu be -> lon:");
		for (int var: arr3) {
			System.out.println(var);
		}
		
	
		Arrays.sort(arr3, new MyComparator());
		System.out.println("Sap xep tu lon -> be:");
		for (int var: arr3) {
			System.out.println(var);
		}
			
	}
}
