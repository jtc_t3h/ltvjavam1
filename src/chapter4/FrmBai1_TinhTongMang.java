package chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai1_TinhTongMang extends JFrame {

	private JPanel contentPane;
	private JTextField txtN;
	private JTextField txtTong;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_TinhTongMang frame = new FrmBai1_TinhTongMang();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_TinhTongMang() {
		setTitle("Tính tổng các phần tử có giá trị ngẫu nhiên trong mảng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 565, 314);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpN = new JLabel("Nhập n:");
		lblNhpN.setBounds(6, 18, 61, 16);
		contentPane.add(lblNhpN);
		
		txtN = new JTextField();
		txtN.setBounds(163, 13, 147, 26);
		contentPane.add(txtN);
		txtN.setColumns(10);
		
		JLabel lblMngcPht = new JLabel("Mảng được phát sinh");
		lblMngcPht.setBounds(6, 60, 147, 16);
		contentPane.add(lblMngcPht);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(163, 57, 383, 129);
		contentPane.add(scrollPane);
		
		JTextArea taArray = new JTextArea();
		taArray.setLineWrap(true);
		taArray.setEditable(false);
		scrollPane.setViewportView(taArray);
		
		JLabel lblTng = new JLabel("Tổng");
		lblTng.setBounds(6, 208, 61, 16);
		contentPane.add(lblTng);
		
		txtTong = new JTextField();
		txtTong.setEditable(false);
		txtTong.setBounds(162, 198, 183, 26);
		contentPane.add(txtTong);
		txtTong.setColumns(10);
		
		JButton btnNewButton = new JButton("Tính Tổng");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sN = txtN.getText().trim();
				int n = Integer.parseInt(sN);
				
				// Sinh ra mảng với các phần tử ngẫu nhiên (số nguyên)
				int arr[] = generateArray(n);
				
				// Tính tổng các phần tử
				int sum = sum(arr);
				
				// Hiển thị ra giao diện
				StringBuilder sb = new StringBuilder();
				for (int element: arr) {
					sb.append(String.valueOf(element)).append(" ");
				}
				taArray.setText(sb.toString());
				txtTong.setText(String.valueOf(sum));
				
			}
		});
		btnNewButton.setBounds(163, 243, 147, 29);
		contentPane.add(btnNewButton);
	}

	/**
	 * Sinh một số ngẫu nhiên thì có 2 cách <br>
	 *	Cách 1: dùng lớp java.util.Random <br>
	 *	Cách 2: dùng lớp java.lang.Math
	 * @param n
	 * @return
	 */
	protected int[] generateArray(int n) {
		int []arr = new int[n];
		
		// Sinh ngẫu nhiên (số nguyên) dùng cách lớp Math
		for(int idx = 0; idx < n; idx++) {
			arr[idx] = (int) (1000 * Math.random());
		}
		
		return arr;
	}

	private int sum(int[] arr) {
		int sum = 0;
		for (int element: arr) {
			sum += element;
		}
		return sum;
	}
}
