package chapter7;

public class TestClient {

	public static void main(String[] args) {

		Integer iArr[] = {1,2,3,4,5};
		Double dArr[] = {1.1, 2.2, 3.3, 4.4, 5.5};
		String sArr[] = {"Hello", "world", "Java"};
		
		printArray(iArr);
		printArray(dArr);
		printArray(sArr);
	}
	
	// Generic method
	public static<E> void printArray(E[] arr) {
		for (E e: arr) {
			System.out.println(e);
		}
	}

}
