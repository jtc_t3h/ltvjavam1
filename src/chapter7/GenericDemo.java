package chapter7;

public class GenericDemo {

	public static void main(String[] args) {
		String [] sArray = {"abc", "def"};
		Double [] dArray = {1.1, 2.2};
		Integer[] iArray = {1, 2};
		
		// Duyet ca phan tu cua 3 mang tren
		printArray(sArray);
		printArray(dArray);
		printArray(iArray);
	}
	
	// Lap trinh Generic -> khai bao cho phuong thuc
	public static <T> void printArray(T [] arr) {
		for (T e: arr) {
			System.out.println(e);
		}
	}
	

//	public static void printArray(String[] arr) {
//		for (String e: arr) {
//			System.out.println(e);
//		}
//	}
//	
//	public static void printArray(Double[] arr) {
//		for (Double e: arr) {
//			System.out.println(e);
//		}
//	}
//	
//	public static void printArray(Integer[] arr) {
//		for (Integer e: arr) {
//			System.out.println(e);
//		}
//	}
}
