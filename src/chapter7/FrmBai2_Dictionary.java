package chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_Dictionary extends JFrame {

	private JPanel contentPane;
	private JTextField txtWord;
	private JTextField txtMeaning;
	
	private List<Bai2_Dictionary> listDic = new ArrayList<Bai2_Dictionary>();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Dictionary frame = new FrmBai2_Dictionary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Dictionary() {
		setTitle("Dictionary");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 660, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 660, 273);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Show - Insert Word", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblWord = new JLabel("Word");
		lblWord.setBounds(6, 20, 61, 16);
		panel_1.add(lblWord);
		
		txtWord = new JTextField();
		txtWord.setBounds(57, 15, 130, 26);
		panel_1.add(txtWord);
		txtWord.setColumns(10);
		
		JLabel lblMeaning = new JLabel("Meaning");
		lblMeaning.setBounds(199, 20, 61, 16);
		panel_1.add(lblMeaning);
		
		txtMeaning = new JTextField();
		txtMeaning.setBounds(271, 15, 226, 26);
		panel_1.add(txtMeaning);
		txtMeaning.setColumns(10);
		
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Tạo đối tượng Dictionary -> lấy dữ liệu người dùng nhập
				String word = txtWord.getText().trim();
				String meaning = txtMeaning.getText().trim();
				Bai2_Dictionary dic = new Bai2_Dictionary(word, meaning);
				
				// Add từ điển vào trong listDic
				listDic.add(dic);
				
				// sort listDic
				listDic.sort(new Comparator<Bai2_Dictionary>() {

					@Override
					public int compare(Bai2_Dictionary dic1, Bai2_Dictionary dic2) {
						int ret = dic1.getWord().compareTo(dic2.getWord());
						if (ret == 0) {
							return dic1.getMeaning().compareTo(dic2.getMeaning());
						}
						return ret;
					}
				});
				
				// Hiển thị danh sách listDic vào JList -> lay model, remove all, add phần tử vào model
				DefaultListModel<Bai2_Dictionary> model = (DefaultListModel<Bai2_Dictionary>) list.getModel();
			    model.removeAllElements();
				for (Bai2_Dictionary dictionary: listDic) {
			    	model.addElement(dictionary);
			    }
			    list.setModel(model);
			}
		});
		btnInsert.setBounds(509, 15, 117, 29);
		panel_1.add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(16, 48, 610, 161);
		panel_1.add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		// Khởi tạo model rỗng (các phần tử có kiểu dữ liệu Bai2_Dictionary)
		DefaultListModel<Bai2_Dictionary> model = new DefaultListModel<Bai2_Dictionary>();
		list.setModel(model);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Search", null, panel, null);
	}
}
