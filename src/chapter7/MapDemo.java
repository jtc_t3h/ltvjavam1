package chapter7;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		Map<String, Integer> map1 = new HashMap<>();
		map1.put("abc", 123);
		map1.put("xyz", 345);
		
		map1.put("abc", 678);
		
		for (String key: map1.keySet()) {
			System.out.println(key + "=" + map1.get(key));
		}
	}

}
