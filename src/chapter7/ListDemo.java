package chapter7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class ListDemo {

	public static void main(String[] args) {

		List<Integer> list1 = new ArrayList<Integer>();
		list1.add(1);
		list1.add(5);
		list1.add(1, 4);
		for (int e: list1) {
			System.out.println(e);
		}
		
		
		List list2 = new Vector();
		list2.add(3);
		list2.add("abc");
		list2.add(1.2);
		for (Object e: list2) {
			System.out.println(e);
		}
	}

}
