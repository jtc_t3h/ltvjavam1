package chapter7;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {

		Set set = new HashSet();
		set.add(123);
		set.add("abc");
		set.add(123);
		
		for (Object e: set) {
			System.out.println(e);
		}
	}

}
