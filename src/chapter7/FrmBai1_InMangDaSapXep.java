package chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;

public class FrmBai1_InMangDaSapXep extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_InMangDaSapXep frame = new FrmBai1_InMangDaSapXep();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_InMangDaSapXep() {
		setTitle("In mảng chuỗi, số nguyên và số thực");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoiMng = new JLabel("Loại mảng");
		lblLoiMng.setBounds(16, 13, 79, 14);
		contentPane.add(lblLoiMng);
		
		JComboBox cbbArray = new JComboBox();
		cbbArray.setModel(new DefaultComboBoxModel(new String[] {"--- Chọn ---", "Chuỗi", "Số nguyên", "Số thực"}));
		cbbArray.setBounds(105, 9, 208, 22);
		contentPane.add(cbbArray);
		
		JLabel lblNewLabel = new JLabel("Nhập mảng các phần tử cách nhau bởi 1 khoảng trắng");
		lblNewLabel.setBounds(16, 52, 416, 14);
		contentPane.add(lblNewLabel);
		
		JTextArea taInput = new JTextArea();
		taInput.setBounds(16, 77, 416, 60);
		contentPane.add(taInput);
		
		JLabel lblMngSauKhi = new JLabel("Mảng sau khi sắp xếp");
		lblMngSauKhi.setBounds(16, 148, 416, 14);
		contentPane.add(lblMngSauKhi);
		
		JTextArea taOutput = new JTextArea();
		taOutput.setBounds(16, 173, 416, 60);
		contentPane.add(taOutput);
		
		JButton btnSort = new JButton("Sắp xếp tăng");
		btnSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Lấy dữ liệu người dùng nhập vào
				String input = taInput.getText().trim();
				
				// Tách chuỗi người dùng nhập thành mảng
				String[] split = input.split(" ");
				
				// Tạo list và thêm các phần tử vào
				List list = new LinkedList();
				for (String element: split) {
					if (cbbArray.getSelectedIndex() == 1) {// Chuoi
						list.add(element);
					} else if (cbbArray.getSelectedIndex() == 2) { // So Nguyen
						list.add(Integer.parseInt(element));
					} else if (cbbArray.getSelectedIndex() == 3) { // So Thuc
						list.add(Double.parseDouble(element));
					}
				}
				
				// Thien hien sap xep
				Collections.sort(list);
				
				// Hien thi ket qua
				taOutput.setText(list.toString());
			}
		});
		btnSort.setBounds(164, 244, 117, 23);
		contentPane.add(btnSort);
	}
}
