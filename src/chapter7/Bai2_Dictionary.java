package chapter7;

public class Bai2_Dictionary {

	private String word;
	private String meaning;
	
	public Bai2_Dictionary(String word, String meaning) {
		this.word = word;
		this.meaning = meaning;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}
	
	@Override
	public String toString() {
		return word + " : " + meaning;
	}
}
