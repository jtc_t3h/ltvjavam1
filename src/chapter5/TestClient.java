package chapter5;

public class TestClient {

	// static method
	public static void main(String[] args) {
		TamGiac tamGiac = new TamGiac();
		TamGiac t2 = new TamGiac(1.1, 2.2, 3.3);
		
		System.out.println(tamGiac.perimeter());
		System.out.println(t2.area());
		
		TestClient test = new TestClient();
		
		int x = 1;
		int [] arrX = {1};
		
		test.changeValue(x); // Gọi hàm -> x là biến thực (đối số thực)
		test.changeValue(arrX);
		
		System.out.println(x); // 1 -> by-pass-value
		System.out.println(arrX[0]); // 11 -> by_pass_reference
		
		varArgsMethod(1);
		varArgsMethod(1,2);
		varArgsMethod(1,2,3,4,5,6,7,8,9,10);
		
		varArgsMethod2("varArgsMethod2",1);
		varArgsMethod2("varArgsMethod2",1,2);
		varArgsMethod2("varArgsMethod2",1,2,3,4,5,6,7,8,9,10);
	}

	// Khai báo hàm -> x là biến hình thức (đối số hình thức)
	private void changeValue(int x) {
		x = x + 10; // 11
	}
	
	private void changeValue(int[] arrX) {
		arrX[0] += 10;
	}
	
	public static void varArgsMethod(int ... arr) {
		// Duyệt danh sách các đối số
		System.out.println("=====================");
		for (int e: arr) {
			System.out.println(e);
		}
	}
	
	public static void varArgsMethod2(String name, int ... arr) {
		System.out.println(name + "=========");
		
		for (int e: arr) {
			System.out.println(e);
		}
	}
}
