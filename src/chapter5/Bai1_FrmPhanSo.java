package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bai1_FrmPhanSo extends JFrame {

	private JPanel contentPane;
	private JTextField txtTuSo1;
	private JTextField txtMauSo1;
	private JTextField txtMauSo2;
	private JTextField txtTuSo2;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai1_FrmPhanSo frame = new Bai1_FrmPhanSo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai1_FrmPhanSo() {
		setTitle("Thực hiện các toán tử trên phân số");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPhnS = new JLabel("Phân số 1");
		lblPhnS.setBounds(66, 11, 64, 14);
		contentPane.add(lblPhnS);
		
		txtTuSo1 = new JTextField();
		txtTuSo1.setBounds(66, 36, 86, 20);
		contentPane.add(txtTuSo1);
		txtTuSo1.setColumns(10);
		
		txtMauSo1 = new JTextField();
		txtMauSo1.setBounds(66, 64, 86, 20);
		contentPane.add(txtMauSo1);
		txtMauSo1.setColumns(10);
		
		JLabel lblTS = new JLabel("Tử số");
		lblTS.setBounds(10, 39, 46, 14);
		contentPane.add(lblTS);
		
		JLabel lblMuS = new JLabel("Mẫu số");
		lblMuS.setBounds(10, 67, 46, 14);
		contentPane.add(lblMuS);
		
		JComboBox cbbOperator = new JComboBox();
		cbbOperator.setModel(new DefaultComboBoxModel(new String[] {"", "+", "-", "*", "/"}));
		cbbOperator.setBounds(187, 45, 53, 22);
		contentPane.add(cbbOperator);
		
		JLabel label = new JLabel("Tử số");
		label.setBounds(266, 39, 46, 14);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Mẫu số");
		label_1.setBounds(266, 67, 46, 14);
		contentPane.add(label_1);
		
		txtMauSo2 = new JTextField();
		txtMauSo2.setColumns(10);
		txtMauSo2.setBounds(322, 64, 86, 20);
		contentPane.add(txtMauSo2);
		
		txtTuSo2 = new JTextField();
		txtTuSo2.setColumns(10);
		txtTuSo2.setBounds(322, 36, 86, 20);
		contentPane.add(txtTuSo2);
		
		JLabel lblPhnS_1 = new JLabel("Phân số 2");
		lblPhnS_1.setBounds(322, 11, 64, 14);
		contentPane.add(lblPhnS_1);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(10, 129, 46, 14);
		contentPane.add(lblKtQu);
		
		txtKQ = new JTextField();
		txtKQ.setEditable(false);
		txtKQ.setColumns(10);
		txtKQ.setBounds(66, 126, 86, 20);
		contentPane.add(txtKQ);
		
		JButton btnTnh = new JButton("Tính");
		btnTnh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Tạo ra 2 phân số từ người dùng nhập
				int tuSo1 = Integer.parseInt(txtTuSo1.getText().trim());
				int mauSo1 = Integer.parseInt(txtMauSo1.getText().trim());
				Bai1_PhanSo ps1 = new Bai1_PhanSo(tuSo1, mauSo1);
				
				Bai1_PhanSo ps2 = new Bai1_PhanSo(Integer.parseInt(txtTuSo2.getText().trim()), Integer.parseInt(txtMauSo2.getText().trim()));
			
				// Kiểm tra tác vụ người dùng chọn và thực tác vụ đó
				Bai1_PhanSo kq = null;
				if ("+".equals(cbbOperator.getSelectedItem())) {
					kq = ps1.add(ps2);
				} else if ("-".equals(cbbOperator.getSelectedItem())) {
					kq = ps1.subtract(ps2);
				} else if ("*".equals(cbbOperator.getSelectedItem())) {
				} else if ("/".equals(cbbOperator.getSelectedItem())) {
				}
				
				// Hiển thị kết quả
				txtKQ.setText(kq.toString());
			}
		});
		btnTnh.setBounds(66, 176, 91, 23);
		contentPane.add(btnTnh);
		
		JButton btnLmLi = new JButton("Làm lại");
		btnLmLi.setBounds(199, 176, 91, 23);
		contentPane.add(btnLmLi);
	}
	
	
}
