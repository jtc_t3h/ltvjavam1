package chapter5;

public class TestMethod {

	public static void main(String[] args) {

		int var = 1;
		int [] arr = {1};
		
		TestMethod test = new TestMethod();
		test.changeValue(var);
		test.changeValue(arr);
		
		// 1: Truyen tham tri
		// 11: truyen tham bien
		System.out.println("var = " + var); 
		System.out.println("arr[0] = " + arr[0]);
		
		test.varLengArgs(1);
		test.varLengArgs(1,2,3,4,5);
		test.varLengArgs();
	}

	public void changeValue(int var) {
		var = var + 10; // 11
	}
	
	public void changeValue(int [] arr) {
		arr[0] += 10;  // arr[0] = 11
	}
	
	public void varLengArgs(int ... varArgs) {
		// Duyệt các phần tử đối số
		System.out.println("=====================");
		for (int e: varArgs) {
			System.out.println(e);
		}
	}
}
