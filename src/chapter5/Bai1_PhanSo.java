package chapter5;

public class Bai1_PhanSo {

	// Biến thực thể
	private int tuSo;
	private int mauSo;

	// Phương thức khởi tạo -> mac dinh tuong minh
	public Bai1_PhanSo() {
	}

	public Bai1_PhanSo(int tuSo, int mauSo) {
		this.tuSo = tuSo;
		this.mauSo = mauSo;
	}

	// Setter và Getter
	public int getTuSo() {
		return tuSo;
	}

	public void setTuSo(int tuSo) {
		this.tuSo = tuSo;
	}

	public int getMauSo() {
		return mauSo;
	}

	public void setMauSo(int mauSo) {
		this.mauSo = mauSo;
	}

	// Hàm nghiệp vụ
	public Bai1_PhanSo add(Bai1_PhanSo ps2) {
		int tuSo = this.tuSo * ps2.getMauSo() + this.mauSo * ps2.getTuSo();
		int mauSo = this.mauSo * ps2.getMauSo();
		return new Bai1_PhanSo(tuSo, mauSo);
	}
	
	public Bai1_PhanSo subtract(Bai1_PhanSo ps2) {
		int tuSo = this.tuSo * ps2.getMauSo() - this.mauSo * ps2.getTuSo();
		int mauSo = this.mauSo * ps2.getMauSo();
		return new Bai1_PhanSo(tuSo, mauSo);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return tuSo + "/" + mauSo;
	}
	
	
}
