package chapter5;

public class TamGiac {

	// instance variable
	private double a;
	private double b;
	private double c;
	
	// ham khoi mac dinh (tuong minh)
	public TamGiac() {
	}
	
	// Hàm khởi tạo có đối số
	public TamGiac(double _a, double _b, double _c) {
		a = _a;
		b = _b;
		c = _c;
	}
	
	// Hàm getter -> định dạng hàm: getVarName với VarName là biến thực thể 
	// nhưng kí tự đầu tiên của biến được viết hoa
	public double getB() {
		return b;
	}

	// Hàm setter
	public void setB(double b) { // b: biến cục bộ (local variable)
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getA() {
		return a;
	}
	
	public void setA(double _a) {
		a = _a;
	}
	
	// Hàm nghiệp vụ (bussines method)
	public double area() {
		return a + b + c;
	}
	
	public double perimeter() {
		return a + b + c;
	}
}
