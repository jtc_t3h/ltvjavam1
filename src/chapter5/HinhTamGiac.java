package chapter5;

public class HinhTamGiac {

	// instance variable
	private double a;
	private double b;
	private double c;
	
	public HinhTamGiac() {
	}
	
	public HinhTamGiac(double _a) {
		a = _a;
	}
	
	public HinhTamGiac(double _a, double _b) {
		a = _a;
		b = _b;
	}

	public HinhTamGiac(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	// Lấy giá của thuộc tính a
	public double getA() {
		return a;
	}
	public void setA(double _a) {
		a = _a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}
	
	public double tinhCV() {
		return a + b + c;
	}
	
	public double tinhDT() {
		return 0.0;
	}
	
	public void aMethod(String name, int ... values) {
		
	}
}
