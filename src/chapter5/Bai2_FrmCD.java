package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import chapter6.Bai1_Cat;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bai2_FrmCD extends JFrame {

	private JPanel contentPane;
	private JTextField txtMaCD;
	private JTextField txtTenCD;
	private JTextField txtCaSi;
	private JTextField txtSoBaiHat;
	private JTextField txtGiaThanh;
	
	private String chuoiCD = "";
	private int tongGiaThanh = 0;
	private JTextArea txtaMang;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai2_FrmCD frame = new Bai2_FrmCD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai2_FrmCD() {
		setTitle("Quản lý CD");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 587, 376);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMCd = new JLabel("Mã CD");
		lblMCd.setBounds(10, 21, 46, 14);
		contentPane.add(lblMCd);
		
		txtMaCD = new JTextField();
		txtMaCD.setBounds(66, 18, 144, 20);
		contentPane.add(txtMaCD);
		txtMaCD.setColumns(10);
		
		JLabel lblTnCd = new JLabel("Tên CD");
		lblTnCd.setBounds(234, 21, 46, 14);
		contentPane.add(lblTnCd);
		
		txtTenCD = new JTextField();
		txtTenCD.setBounds(290, 18, 271, 20);
		contentPane.add(txtTenCD);
		txtTenCD.setColumns(10);
		
		JLabel lblCaS = new JLabel("Ca sĩ");
		lblCaS.setBounds(10, 56, 46, 14);
		contentPane.add(lblCaS);
		
		txtCaSi = new JTextField();
		txtCaSi.setBounds(66, 53, 167, 20);
		contentPane.add(txtCaSi);
		txtCaSi.setColumns(10);
		
		JLabel lblSBiHt = new JLabel("Số bài hát");
		lblSBiHt.setBounds(244, 56, 63, 14);
		contentPane.add(lblSBiHt);
		
		txtSoBaiHat = new JTextField();
		txtSoBaiHat.setBounds(317, 53, 63, 20);
		contentPane.add(txtSoBaiHat);
		txtSoBaiHat.setColumns(10);
		
		JLabel lblGiThnh = new JLabel("Giá thành");
		lblGiThnh.setBounds(390, 56, 63, 14);
		contentPane.add(lblGiThnh);
		
		txtGiaThanh = new JTextField();
		txtGiaThanh.setBounds(456, 53, 105, 20);
		contentPane.add(txtGiaThanh);
		txtGiaThanh.setColumns(10);
		
		JButton btnAdd = new JButton("Thêm mới");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// B1. Lấy dữ liệu người dùng nhập
				String maCD = txtMaCD.getText().trim();
				String tenCD = txtTenCD.getText().trim();
				String caSi = txtCaSi.getText().trim();
				int soBaiHat = Integer.valueOf(txtSoBaiHat.getText().trim());
				int giaThanh = Integer.parseInt(txtGiaThanh.getText().trim());
				
				// B2. Tạo ra đối tượng Bai1_CD
				Bai2_CD cd = new Bai2_CD();
				cd.setId(maCD);
				cd.setName(tenCD);
				cd.setSinger(caSi);
				cd.setNumOfSubject(soBaiHat);
				cd.setPrice(giaThanh);
				
				// B3. Thêm thông tin CD vào chuoiCD hiện tại và cập nhật tổng giá thành
				chuoiCD += cd.toString() + "\n";
				tongGiaThanh +=  cd.getPrice();
				
				// B4. Hiển thị ra txta (textArea)
				txtaMang.setText(chuoiCD + "\n Tổng giá thành = " + tongGiaThanh );
			}
		});
		btnAdd.setBounds(159, 99, 105, 23);
		contentPane.add(btnAdd);
		
		JButton btnClear = new JButton("Tiếp tục");
		btnClear.setBounds(274, 99, 89, 23);
		contentPane.add(btnClear);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 140, 551, 187);
		contentPane.add(scrollPane);
		
		txtaMang = new JTextArea();
		scrollPane.setViewportView(txtaMang);
	}
}
