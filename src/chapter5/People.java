package chapter5;

import java.util.Date;

public class People {

	// instance variable
	private int id;
	private String fullName;
	private boolean gender; // true (Nam), false (Nu)
	private Date birthday;
	
	// Hàm khởi tạo
	public People() {}
	
	public People(int _id) {
		id = _id;
	}
	
	// Getter
	public int getId() {
		return id;
	}
	
	// Setter
	public void setId(int _id) {
		id = _id;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	// Ham nghiep vu
	public void printInfo() {
		System.out.println("Full Name: " + fullName);
		System.out.println("ID: = " + id);
	}

	public static void main(String[] args) {
		People people = new People();
	}
}
