package chapter5;

public class Bai2_CD {

	// Instance variable
	private String id;
	private String name;
	private String singer;
	private int numOfSubject;
	private int price;
	
	public Bai2_CD() {
	}
	
	public Bai2_CD(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public Bai2_CD(String _id, String _name, String _singer, int _numOfSubject, int _price) {
		id = _id;
		name = _name;
		singer = _singer;
		numOfSubject = _numOfSubject;
		price = _price;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public int getNumOfSubject() {
		return numOfSubject;
	}

	public void setNumOfSubject(int numOfSubject) {
		this.numOfSubject = numOfSubject;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	// Business method
	@Override // nạp chồng hàm
	public String toString() {
		return id + " - " + name + " - " + singer + " - " + numOfSubject + " - " + price;
	}
}
