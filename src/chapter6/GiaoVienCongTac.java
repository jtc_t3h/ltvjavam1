package chapter6;

public class GiaoVienCongTac extends GiaoVien {

	private int soTiet;
	private double donGia;

	public int getSoTiet() {
		return soTiet;
	}

	public void setSoTiet(int soTiet) {
		this.soTiet = soTiet;
	}

	public double getDonGia() {
		return donGia;
	}

	public void setDonGia(double donGia) {
		this.donGia = donGia;
	}

	@Override
	public double tinhLuong() {
		return soTiet * donGia;
	}

}
