package chapter6;

public class Bai2_NhanVienHanhChanh extends Bai2_NhanVien {

	private int phuCap;

	public int getPhuCap() {
		return phuCap;
	}

	public void setPhuCap(int phuCap) {
		this.phuCap = phuCap;
	}
	
	@Override
	public double tinhLuong() {
		return super.tinhLuong() + phuCap;
	}
}
