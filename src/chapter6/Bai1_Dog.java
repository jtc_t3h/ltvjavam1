package chapter6;

public class Bai1_Dog extends Bai1_Animal{
	
	public Bai1_Dog(String name, String images) {
		super(name, images);
	}

	@Override
	public String sayHello() {
		return  super.getName() + "say Gâu gâu gâu";
	}
}
