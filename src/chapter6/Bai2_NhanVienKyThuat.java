package chapter6;

public class Bai2_NhanVienKyThuat extends Bai2_NhanVien{

	private int soDuAn;

	public int getSoDuAn() {
		return soDuAn;
	}

	public void setSoDuAn(int soDuAn) {
		this.soDuAn = soDuAn;
	}
	
	@Override
	public double tinhLuong() {
		return super.tinhLuong() + soDuAn * 1000000;
	}
}
