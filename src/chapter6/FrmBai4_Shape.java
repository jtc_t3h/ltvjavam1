package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FrmBai4_Shape extends JFrame {

	private JPanel contentPane;
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JTextField txtRadius;
	private JTextField txtChuVi;
	private JTextField txtDienTich;
	private JTextField txtChieudai;
	private JTextField txtChieurong;
	private JTextField txtChuVi2;
	private JTextField txtDienTich2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_Shape frame = new FrmBai4_Shape();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_Shape() {
		setTitle("Abstract Class");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		tabbedPane.setBounds(0, 0, 442, 273);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Hinh Tròn", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bán kính");
		lblNewLabel.setBounds(10, 11, 95, 14);
		panel.add(lblNewLabel);
		
		txtRadius = new JTextField();
		txtRadius.setBounds(115, 8, 181, 20);
		panel.add(txtRadius);
		txtRadius.setColumns(10);
		
		txtChuVi = new JTextField();
		txtChuVi.setEnabled(false);
		txtChuVi.setColumns(10);
		txtChuVi.setBounds(115, 39, 181, 20);
		panel.add(txtChuVi);
		
		txtDienTich = new JTextField();
		txtDienTich.setEnabled(false);
		txtDienTich.setColumns(10);
		txtDienTich.setBounds(115, 70, 181, 20);
		panel.add(txtDienTich);
		
		JLabel lblTnhChuVi = new JLabel("Tính chu vi");
		lblTnhChuVi.setBounds(10, 42, 95, 14);
		panel.add(lblTnhChuVi);
		
		JLabel lblTnhDinTch = new JLabel("Tính diện tích");
		lblTnhDinTch.setBounds(10, 76, 95, 14);
		panel.add(lblTnhDinTch);
		
		JButton btnTnhCv = new JButton("Tính CV");
		btnTnhCv.setBounds(306, 38, 91, 23);
		panel.add(btnTnhCv);
		
		JButton btnTnhDt = new JButton("Tính DT");
		btnTnhDt.setBounds(306, 69, 91, 23);
		panel.add(btnTnhDt);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Hình Chữ Nhật", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Chiều dài");
		lblNewLabel_1.setBounds(10, 11, 88, 14);
		panel_1.add(lblNewLabel_1);
		
		txtChieudai = new JTextField();
		txtChieudai.setBounds(108, 8, 208, 20);
		panel_1.add(txtChieudai);
		txtChieudai.setColumns(10);
		
		JLabel lblChiuRng = new JLabel("chiểu rộng");
		lblChiuRng.setBounds(10, 39, 88, 14);
		panel_1.add(lblChiuRng);
		
		txtChieurong = new JTextField();
		txtChieurong.setColumns(10);
		txtChieurong.setBounds(108, 36, 208, 20);
		panel_1.add(txtChieurong);
		
		JLabel lblChuVi = new JLabel("Chu vi");
		lblChuVi.setBounds(10, 67, 88, 14);
		panel_1.add(lblChuVi);
		
		txtChuVi2 = new JTextField();
		txtChuVi2.setColumns(10);
		txtChuVi2.setBounds(108, 64, 208, 20);
		panel_1.add(txtChuVi2);
		
		JLabel lblDinTch = new JLabel("Diện tích");
		lblDinTch.setBounds(10, 95, 88, 14);
		panel_1.add(lblDinTch);
		
		txtDienTich2 = new JTextField();
		txtDienTich2.setColumns(10);
		txtDienTich2.setBounds(108, 92, 208, 20);
		panel_1.add(txtDienTich2);
		
		JButton btnTnhCv_1 = new JButton("Tính CV");
		btnTnhCv_1.setBounds(324, 63, 91, 23);
		panel_1.add(btnTnhCv_1);
		
		JButton btnTnhDt_1 = new JButton("Tính DT");
		btnTnhDt_1.setBounds(326, 91, 91, 23);
		panel_1.add(btnTnhDt_1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("New tab", null, panel_2, null);
	}
}
