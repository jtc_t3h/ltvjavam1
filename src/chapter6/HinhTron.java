package chapter6;

public class HinhTron extends HinhHoc{
	
	private double r;
	
	public HinhTron() {
		super();
	}
	
	public HinhTron(double r) {
		super("HinhTrong");
		this.r = r;
	}

	@Override
	public double tinhCV() {
		super.tinhCV();
		
		System.out.println("HinhTron: tinhCV.");
		return 0.0;
	}
	
	@Override
	public Double tinhDT() {
		return 0.0;
	}
}
