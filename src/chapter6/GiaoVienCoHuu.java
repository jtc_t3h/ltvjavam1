package chapter6;

public class GiaoVienCoHuu extends GiaoVien {

	private double luongCB;
	private double heSo;

	public double getLuongCB() {
		return luongCB;
	}

	public void setLuongCB(double luongCB) {
		this.luongCB = luongCB;
	}

	public double getHeSo() {
		return heSo;
	}

	public void setHeSo(double heSo) {
		this.heSo = heSo;
	}

	@Override
	public double tinhLuong() {
		return luongCB * heSo;
	}

}
