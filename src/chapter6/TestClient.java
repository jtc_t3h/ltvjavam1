package chapter6;

public class TestClient {
	
	// static block
	static {
		System.out.println("Static block.");
	}

	public static void main(String[] args) {
		
		// Tính đa hình: compile và runtime
		HinhHoc hh = new HinhHoc(); // hh: Hinh Hoc
		hh.toString();
		
		hh.tinhCV();
		
		hh = new HinhTron(); // hh: Hinh Tron
		hh.tinhCV();
		
		hh = new HinhTamGiac(); // hh: HinhTamGiac
		hh.tinhCV(); 
		
		hh = new HinhChuNhat(); // hh: HinhChuNhat
		hh.tinhCV();
		
		TestClient test = new TestClient();
		test.aMethod(1.0F);
		
		People p1 = new People();
		People p2 = new People();		
		
		p1.total = 10;
		System.out.println(People.total);
//		
//		ShapeInt s = new Circle();
		
		HinhTamGiac tg = new HinhTamGiac();
		ShapeInt shape = new Circle();
		
		GiaoVien giaoVien = new GiaoVienCoHuu();
	}

	public void aMethod() {}
	
	public void aMethod(String aP) {}
	
	public void aMethod(int aP) {}
	
	public int aMethod(float aP) {
		return 0;
	}
	
}
