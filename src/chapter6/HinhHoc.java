package chapter6;

public class HinhHoc {
	
	// Biến thực thể
	private String name;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HinhHoc() {
		System.out.println("HinhHoc contructor with no-args.");
	}
	
	public HinhHoc(String s) {
		System.out.println("HinhHoc contructor with 1-args.");
	}

	protected double tinhCV() {
		System.out.println("HinhHoc: tinhCV.");
		return 0.0;
	}
	
	public int tinhCV(int type) {
		this.tinhCV();
		return 0;
	}
	
	public int tinhCV(String type) {
		return 0;
	}

	public Number tinhDT() {
		return 0;
	}
	
}
