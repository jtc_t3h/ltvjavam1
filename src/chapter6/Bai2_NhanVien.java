package chapter6;

public class Bai2_NhanVien {

	// instance variable 
	protected final int luongCoBan =  1550000;
	private double heSo;
	
	public double getHeSo() {
		return heSo;
	}
	public void setHeSo(double heSo) {
		this.heSo = heSo;
	}
	
	// Business method
	public double tinhLuong() {
		return luongCoBan * heSo;
	}
}
