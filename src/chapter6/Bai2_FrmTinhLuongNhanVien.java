package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bai2_FrmTinhLuongNhanVien extends JFrame {

	private JPanel contentPane;
	private JTextField txt2;
	private JTextField txt3;
	private JTextField txtLuong;
	private JLabel lbl2;
	private JLabel lbl3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai2_FrmTinhLuongNhanVien frame = new Bai2_FrmTinhLuongNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai2_FrmTinhLuongNhanVien() {
		setTitle("Tính lương nhân viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 209);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Loại NV");
		lblNewLabel.setBounds(10, 11, 85, 14);
		contentPane.add(lblNewLabel);
		
		JComboBox cbbLoaiNV = new JComboBox();
		cbbLoaiNV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (cbbLoaiNV.getSelectedIndex() == 0) {// Hanh Chanh
					lbl3.setVisible(true);
					txt3.setVisible(true);
					lbl3.setText("Phụ cấp");
				} else if (cbbLoaiNV.getSelectedIndex() == 1) { // Ky thuat
					lbl3.setVisible(true);
					txt3.setVisible(true);
					lbl3.setText("Số dự án");
				} else if (cbbLoaiNV.getSelectedIndex() == 2) { // Kinh doanh
					lbl3.setVisible(true);
					txt3.setVisible(true);
					lbl3.setText("Số sản phẩm");
				} else if (cbbLoaiNV.getSelectedIndex() == 3) { // Lanh dao
					lbl3.setVisible(false);
					txt3.setVisible(false);
				}
			}
		});
		cbbLoaiNV.setModel(new DefaultComboBoxModel(new String[] {"Hành chánh", "Kỹ thuật", "Kinh doanh", "Lãnh đạo"}));
		cbbLoaiNV.setBounds(105, 7, 266, 22);
		contentPane.add(cbbLoaiNV);
		
		lbl2 = new JLabel("Hệ số lương");
		lbl2.setBounds(10, 36, 85, 14);
		contentPane.add(lbl2);
		
		txt2 = new JTextField();
		txt2.setBounds(105, 33, 266, 20);
		contentPane.add(txt2);
		txt2.setColumns(10);
		
		lbl3 = new JLabel("Phụ cấp");
		lbl3.setBounds(10, 64, 85, 14);
		contentPane.add(lbl3);
		
		txt3 = new JTextField();
		txt3.setColumns(10);
		txt3.setBounds(105, 61, 266, 20);
		contentPane.add(txt3);
		
		JButton btnNewButton = new JButton("Tính Lương");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Bai2_NhanVien nhanVien = null;
				if (cbbLoaiNV.getSelectedIndex() == 0) {// Hanh Chanh
					nhanVien = new Bai2_NhanVienHanhChanh();
					((Bai2_NhanVienHanhChanh)nhanVien).setPhuCap(Integer.parseInt(txt3.getText()));
				} else if (cbbLoaiNV.getSelectedIndex() == 1) { // Ky thuat
					nhanVien = new Bai2_NhanVienKyThuat();
					((Bai2_NhanVienKyThuat)nhanVien).setSoDuAn(Integer.parseInt(txt3.getText()));
				} else if (cbbLoaiNV.getSelectedIndex() == 2) { // Kinh doanh
					nhanVien = new Bai2_NhanVienKinhDoanh();
					((Bai2_NhanVienKinhDoanh)nhanVien).setSoSanPham(Integer.parseInt(txt3.getText()));
				} else if (cbbLoaiNV.getSelectedIndex() == 3) { // Lanh dao
					nhanVien = new Bai2_LanhDao();
				}
				nhanVien.setHeSo(Double.parseDouble(txt2.getText().trim()));
				txtLuong.setText(String.valueOf(nhanVien.tinhLuong()));
			}
		});
		btnNewButton.setBounds(105, 93, 105, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblLng = new JLabel("Lương");
		lblLng.setBounds(10, 140, 85, 14);
		contentPane.add(lblLng);
		
		txtLuong = new JTextField();
		txtLuong.setColumns(10);
		txtLuong.setBounds(105, 137, 266, 20);
		contentPane.add(txtLuong);
	}
}
