package chapter6;

public class Bai4_HinhTron extends Bai4_HinhHoc {

	private double r;
	
	public Bai4_HinhTron() {
	}
	
	public Bai4_HinhTron(double r){
		this.r = r;
	}
	
	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	@Override
	public double tinhCV() {
		return r*2*Math.PI;
	}

	@Override
	public double tinhDT() {
		return r*r*Math.PI;
	}

}
