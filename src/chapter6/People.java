package chapter6;

public class People {

	// instance variable
	private int id;
	private String name;
	
	// static variable
	public static int total;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getTotal() {
		return total;
	}

	public static void setTotal(int total) {
		People.total = total;
	}
	
	
}
