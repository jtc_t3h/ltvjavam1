package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Bai1_FrmAnimal extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JLabel lblSayHello;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai1_FrmAnimal frame = new Bai1_FrmAnimal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai1_FrmAnimal() {
		setTitle("Animal say Hello");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 433, 251);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 46, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(75, 8, 145, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblAnimal = new JLabel("Animal");
		lblAnimal.setBounds(10, 36, 46, 14);
		contentPane.add(lblAnimal);
		
		JComboBox cbbAnimal = new JComboBox();
		cbbAnimal.setModel(new DefaultComboBoxModel(new String[] {"Dog", "Cat", "Pig"}));
		cbbAnimal.setBounds(75, 32, 145, 22);
		contentPane.add(cbbAnimal);
		
		JButton btnSayHello = new JButton("Say Hello");
		btnSayHello.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bai1_Animal animal = null;
				
				// Người dùng nhập tên con vật
				String name = txtName.getText().trim();
				switch (cbbAnimal.getSelectedIndex()) {
				case 0: // Dog
					animal = new Bai1_Dog(name, "src/images/tuat.jpg");
					break;
				case 1: // Cat
					
				default: // Pig
					
					break;
				}
				
				lblSayHello.setText(animal.sayHello());
				lblImage.setIcon(new ImageIcon(animal.getImages()));
			}
		});
		btnSayHello.setBounds(75, 82, 91, 23);
		contentPane.add(btnSayHello);
		
		lblSayHello = new JLabel("New label");
		lblSayHello.setForeground(Color.BLUE);
		lblSayHello.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSayHello.setBounds(10, 129, 239, 14);
		contentPane.add(lblSayHello);
		
		lblImage = new JLabel("");
		lblImage.setBounds(230, 11, 177, 188);
		contentPane.add(lblImage);
	}

}
