package chapter6;

public class Bai1_Animal {

	private String name;
	private String images;
	
	private int weight;
	
	public Bai1_Animal() {
	}

	public Bai1_Animal(String name, String images) {
		this.name = name;
		this.images = images;
		
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String sayHello() {
		return "Hello!";
	}
}
