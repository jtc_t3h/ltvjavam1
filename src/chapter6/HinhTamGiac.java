package chapter6;

public class HinhTamGiac extends HinhHoc{
	
	private double a;
	private double b;
	private double c;
	
	public HinhTamGiac() {
		super();
		System.out.println("HinhTamGiac() constructor");
	}

	HinhTamGiac(double a, double b, double c){
		super("HinhTamGiac");
	}

	// Annotation
	@Override
	public double tinhCV() {
		System.out.println("HinhTamGiac: tinhCV.");
		super.tinhCV("HinhTamGiac");
		return a + b + c;
	}
	
	public void aMethod() {
		
	}
	
}
