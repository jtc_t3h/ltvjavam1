package chapter6;

public interface ShapeInt {
	int i = 10;  // constant
	
	public double tinhCV(); // abstract method
	
	
	// Java 8 -> bổ sung thêm hàm hiện thực -> static method và default method
	public static void staticMethod() {
		
	}
	
	public default void defaultMethod() {
		
	}
}
