package chapter6;

public class Bai2_NhanVienKinhDoanh extends Bai2_NhanVien {

	private int soSanPham;

	public int getSoSanPham() {
		return soSanPham;
	}

	public void setSoSanPham(int soSanPham) {
		this.soSanPham = soSanPham;
	}
	
	@Override
	public double tinhLuong() {
		return super.tinhLuong() + soSanPham * 20000;
	}
}
