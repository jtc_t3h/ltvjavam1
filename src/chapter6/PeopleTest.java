package chapter6;

public class PeopleTest {
	
	static {
		System.out.println("static block.");
	}

	public static void main(String[] args) {
		People p1 = new People();
		People p2 = new People();

		System.out.println(p1.total);
		System.out.println(p2.total);
		
		p1.total = 10;
		
		System.out.println(People.total);
		
		PeopleTest test = new PeopleTest();
		test.callMethod();
		
		callMethod();
	}
	
	public static void callMethod() {
		System.out.println("Hello, how are you?");
	}

}
