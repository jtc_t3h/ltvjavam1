package chapter6;

public abstract class GiaoVien {

	private String id;
	private String name;

	public abstract double tinhLuong();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
