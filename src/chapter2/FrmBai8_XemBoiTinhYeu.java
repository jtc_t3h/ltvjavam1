package chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai8_XemBoiTinhYeu extends JFrame {

	private JPanel contentPane;
	private JTextField txtName1;
	private JTextField txtName2;
	
	private final int initScore = 30;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai8_XemBoiTinhYeu frame = new FrmBai8_XemBoiTinhYeu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai8_XemBoiTinhYeu() {
		setTitle("Love Level");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEnterName = new JLabel("Enter names");
		lblEnterName.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		lblEnterName.setBounds(19, 6, 104, 24);
		contentPane.add(lblEnterName);
		
		JLabel lblKetQua = new JLabel("");
		lblKetQua.setHorizontalAlignment(SwingConstants.CENTER);
		lblKetQua.setFont(new Font("Lucida Grande", Font.BOLD, 50));
		lblKetQua.setForeground(Color.WHITE);
		lblKetQua.setBounds(182, 165, 97, 45);
		contentPane.add(lblKetQua);
		
		txtName1 = new JTextField();
		txtName1.setBounds(54, 61, 130, 26);
		contentPane.add(txtName1);
		txtName1.setColumns(10);
		
		txtName2 = new JTextField();
		txtName2.setColumns(10);
		txtName2.setBounds(269, 61, 130, 26);
		contentPane.add(txtName2);
		
		JButton btnNewButton = new JButton("Compute");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name1 = txtName1.getText().trim();
				String name2 = txtName2.getText().trim();
				
//				int kq = lovelevelCompute(name1, name2,);
				lovelevelCompute(name1, name2, lblKetQua);
				
//				lblKetQua.setText(String.valueOf(kq));
			}

			private void lovelevelCompute(String name1, String name2, JLabel lblKetQua) {
				int kq = 0;
				
				if (name1.length() > name2.length()) {
					kq = initScore + (name1.length() - name2.length()) * 10;
				} else if (name2.length() >= name1.length()) {
					kq = initScore + (name2.length() - name1.length()) * 10 + 15;
				} else if (name1.contains(name2)) {
					kq = initScore + 25;
				} else if (name2.contains(name2)) {
					kq = initScore + 30;
				} else if (name1.equalsIgnoreCase(name2)) {
					kq = initScore + 5;
				}
				
				lblKetQua.setText(String.valueOf(kq));
			}
			
			
		});
		btnNewButton.setBounds(166, 231, 117, 29);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmBai8_XemBoiTinhYeu.class.getResource("/images/love.jpg")));
		lblNewLabel.setBounds(0, 0, 450, 278);
		contentPane.add(lblNewLabel);
	}

	private void lovelevelCompute(String name1, String name2) {
		int kq = 0;
		
		if (name1.length() > name2.length()) {
			kq = initScore + (name1.length() - name2.length()) * 10;
		} else if (name2.length() >= name1.length()) {
			kq = initScore + (name2.length() - name1.length()) * 10 + 15;
		} else if (name1.contains(name2)) {
			kq = initScore + 25;
		} else if (name2.contains(name2)) {
			kq = initScore + 30;
		} else if (name1.equalsIgnoreCase(name2)) {
			kq = initScore + 5;
		}
		
//		return kq;
	}
}
