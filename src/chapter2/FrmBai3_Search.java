package chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_Search extends JFrame {

	private JPanel contentPane;
	private JTextField txtS1;
	private JTextField txtS2;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_Search frame = new FrmBai3_Search();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_Search() {
		setTitle("Tìm kiếm chuỗi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 565, 170);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nhập chuỗi 1:");
		lblNewLabel.setBounds(6, 16, 118, 16);
		contentPane.add(lblNewLabel);
		
		txtS1 = new JTextField();
		txtS1.setBounds(136, 11, 423, 26);
		contentPane.add(txtS1);
		txtS1.setColumns(10);
		
		txtS2 = new JTextField();
		txtS2.setColumns(10);
		txtS2.setBounds(136, 39, 423, 26);
		contentPane.add(txtS2);
		
		JLabel lblNhpChui = new JLabel("Nhập chuỗi 2:");
		lblNhpChui.setBounds(6, 44, 118, 16);
		contentPane.add(lblNhpChui);
		
		txtKQ = new JTextField();
		txtKQ.setColumns(10);
		txtKQ.setBounds(136, 67, 423, 26);
		contentPane.add(txtKQ);
		
		JLabel lblKtQu = new JLabel("Kết quả:");
		lblKtQu.setBounds(6, 72, 118, 16);
		contentPane.add(lblKtQu);
		
		JButton btnNewButton = new JButton("Tìm kiếm");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// B1. Lấy dữ liệu người dùng nhập
				String s1 = txtS1.getText().trim();
				String s2 = txtS2.getText().trim();
				
				// B2. Tìm s2 có trong s1
				boolean isKQ = false;
				if (s1.indexOf(s2) != -1) {
					isKQ = true;
				}
				
				// B3. Hiển thị kết quả
				if (isKQ) {
					txtKQ.setText("OK");
				} else {
					txtKQ.setText("Not OK");
				}
			}
		});
		btnNewButton.setBounds(217, 109, 117, 29);
		contentPane.add(btnNewButton);
	}
}
