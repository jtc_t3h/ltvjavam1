package chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class FrmBai7_Zodiac extends JFrame {

	private JPanel contentPane;
	private JTextField txtYear;
	private JLabel lblCan;
	private JLabel lblChi;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai7_Zodiac frame = new FrmBai7_Zodiac();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai7_Zodiac() {
		setTitle("Zodiac");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 288);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblYearOfBirth = new JLabel("Year of Birth");
		lblYearOfBirth.setBounds(37, 20, 88, 16);
		contentPane.add(lblYearOfBirth);

		txtYear = new JTextField();
		txtYear.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		txtYear.setBounds(138, 11, 130, 34);
		contentPane.add(txtYear);
		txtYear.setColumns(10);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int year = Integer.parseInt(txtYear.getText().trim());

				// Xac dinh Can
				switch (year % 10) {
				case 0:
					lblCan.setText("Canh");
					break;
				case 1:

					break;
				case 2:

					break;
				case 3:

					break;
				case 4:

					break;
				case 5:

					break;
				case 6:

					break;
				case 7:

					break;
				case 8:

					break;

				case 9:

					break;
				}
				
				switch (year % 10) {
				case 0:
					lblChi.setText("Thân");
					lblImage.setIcon(new ImageIcon("src/images/than.jpg"));
					break;
				case 1:

					break;
				case 2:

					break;
				case 3:

					break;
				case 4:

					break;
				case 5:

					break;
				case 6:

					break;
				case 7:

					break;
				case 8:

					break;

				case 9:

					break;
				}
			}
			
		});
		btnOk.setBounds(8, 48, 436, 29);
		contentPane.add(btnOk);

		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/images/ti.jpg")));
		lblImage.setBounds(155, 89, 99, 106);
		contentPane.add(lblImage);

		lblCan = new JLabel("");
		lblCan.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCan.setForeground(Color.RED);
		lblCan.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		lblCan.setBounds(132, 218, 68, 21);
		contentPane.add(lblCan);

		lblChi = new JLabel("");
		lblChi.setHorizontalAlignment(SwingConstants.LEFT);
		lblChi.setForeground(Color.RED);
		lblChi.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		lblChi.setBounds(212, 218, 68, 21);
		contentPane.add(lblChi);
	}
}
