package chapter2;

import java.util.StringTokenizer;

public class Demo {

	public static void main(String[] args) {
		
//		// Character
//		char a1 = 'a';
//		char a2 = 97;
//		Character a = new Character('a');
//		
//		System.out.println(Character.isDigit(a1)); // false
//		
//		// String
//		String s1 = "hello";
//		String s2 = new String("hello");
//		String s3 = "hello";
//		
//		System.out.println(s1 == s2);  // false
//		System.out.println(s1.equals(s2)); // true
//		System.out.println(s1 == s3); // true
//		
//		String s = "hello";
//		s.concat(" world");
//		System.out.println(s);
//		
//		for (int idx = 0; idx < 100; idx++) {
//			s += "a";
//		}
//		System.out.println(s);
//		
//		// StringBuilder
//		StringBuilder sb = new StringBuilder("hello");
//		sb.append(" world");
//		sb.reverse();
//		System.out.println(sb.toString());
		
		// StringTokenizer
		String str = "Lap Trinh Java Core";
		StringTokenizer st = new StringTokenizer(str, " ");
		
//		int countToken = st.countTokens();
//		for (int idx = 0; idx < countToken; idx++) {
//			System.out.println(st.nextToken());
//		}
		
		while (st.hasMoreTokens()) {
			System.out.println(st.nextToken());
		}
		
	}

}
