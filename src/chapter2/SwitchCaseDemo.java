package chapter2;

public class SwitchCaseDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int birthday = 1992;
		int iCan = birthday % 10;
		
		String sCan = null;
		switch (iCan) {
		case 0:
			sCan = "Canh";
			break;
		case 1:
			sCan = "Tân";
			break;
		case 2:
			sCan = "Nhâm";
			break;
		case 3:
			sCan = "Quý";
			break;
		case 4:
			sCan = "Giáp";
			break;
		case 5:
			sCan = "Ất";
			break;
		case 6:
			sCan = "Bính";
			break;
		case 7:
			sCan = "Đinh";
			break;
		case 8:
			sCan = "Mậu";
			break;
		case 9:
			sCan = "Kỷ";
			break;
		}
		System.out.println(sCan); // Nhâm | Kỷ | toàn bộ		
	}

}
