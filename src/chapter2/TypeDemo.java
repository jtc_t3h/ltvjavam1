package chapter2;

public class TypeDemo {

	private static int total;
	
	public final float a = 3.14F;
	
	public static void main(String[] args) {
		
		// Primary type
		int i = 10;
		byte b = 100;
		boolean bo = true; // khong gan = 1
		
		// char type
		char cA = 'A';
		char iA = 65;
		
		System.out.println(iA);
		
		// Convert data type
		// 1. Explicit data type
		b = (byte)i; // compile time error
		
		// 2. Implicit data type
		i = b;
		
		// Declare variable
		long l;
		double d = 3.14;
		
	}

}
