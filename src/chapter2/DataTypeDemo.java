package chapter2;

public class DataTypeDemo {

	private String name;
	private static int total;
	
	public static void main(String[] args) {
		System.out.println("Hello world.");
		System.out.println(123456);
		
		char a = 'a';
		float pi = 3.14f;
		byte b = 100; // Giá trị lớn nhất của byte = 127 
		int i = 10;
		
		i = b; // implicit type -> chuyển từ kiểu dữ liệu nhỏ -> lớn
		b = (byte)i; // explicit type -> chuyển từ kiểu dữ liệu lớn -> nhỏ
		
		final String s = "Hello world.";
		
	}
	
	public void aMethod() {
		
	}

}
