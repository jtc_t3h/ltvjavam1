package chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_TinhGiaiThua extends JFrame {

	private JPanel contentPane;
	private JTextField txtX;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_TinhGiaiThua frame = new FrmBai2_TinhGiaiThua();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_TinhGiaiThua() {
		setTitle("Tính giai thừa");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 307, 144);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nhập x:");
		lblNewLabel.setBounds(10, 11, 78, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Kết quả:");
		lblNewLabel_1.setBounds(10, 36, 78, 14);
		contentPane.add(lblNewLabel_1);

		txtX = new JTextField();
		txtX.setBounds(114, 8, 153, 20);
		contentPane.add(txtX);
		txtX.setColumns(10);

		txtKQ = new JTextField();
		txtKQ.setEditable(false);
		txtKQ.setBounds(114, 33, 153, 20);
		contentPane.add(txtKQ);
		txtKQ.setColumns(10);

		JButton btnNewButton = new JButton("Tính");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Bước 1: Lấy dữ liệu người dùng nhập -> Xử lý chuẩn h
				String sX = txtX.getText().trim();
				int x = Integer.valueOf(sX);

				// Bước 2: Thực hiện giải thuật tính giai thừa
				long kq = tinhGiaiThua(x);

				// Bước 3: Hiện thị kết quả
				txtKQ.setText(String.valueOf(kq));
			}
		});
		btnNewButton.setBounds(35, 71, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Nhập lại");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtX.setText("");
				txtKQ.setText("");
			}
		});
		btnNewButton_1.setBounds(154, 71, 89, 23);
		contentPane.add(btnNewButton_1);
	}

	private Long tinhGiaiThua(int x) {
		Long kq = 1l;
		if (x < 0) {
			return -1l;
		}
		for (int i = 1; i <= x; i++) {
			kq = kq * i;
		}
		return kq;
	}
}
