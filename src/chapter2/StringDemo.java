package chapter2;

import java.util.StringTokenizer;

public class StringDemo {

	public static void main(String[] args) {

		// Tạo đối tượng của lớp String
		String s1 = "hello";
		String s2 = new String("hello");
		String s3 = "hello";
		
		// Immutable
		System.out.println("######## Demo: Immutable ##########");
		s1.concat(" world");
//		s1 = s1.concat(" world");
		System.out.println(s1); // expected: Hello world
		for (int idx = 0; idx < 100; idx++) {
			s1.concat(String.valueOf(idx));
		}
	
		
		System.out.println(s1 == s2); // true | false (2)
		System.out.println(s2.equals(s3)); // true
		System.out.println(s1 == s3);
		
//		s1 = s1.concat(" world");
//		System.out.println(s1);
//		
//		String s = "Lap trinh java";
//		StringTokenizer st = new StringTokenizer(s, " ");
//		
//		// Duyet danh sach token
//		int countTokens = st.countTokens();
//		for (int idx = 0; idx < countTokens; idx++) {
//			System.out.println(st.nextToken());
//		}
//		
//		while(st.hasMoreTokens()) {
//			System.out.println(st.nextToken());
//		}
	}

}
