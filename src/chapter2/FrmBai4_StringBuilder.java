package chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class FrmBai4_StringBuilder extends JFrame {

	private JPanel contentPane;
	private JTextField txtSb;
	private JTextField txtSb1;
	private JTextField txtSb2;
	private JTextField txtInsert;
	private JTextField txtFirst;
	private JTextField txtEnd;
	private JTextArea taResult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_StringBuilder frame = new FrmBai4_StringBuilder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_StringBuilder() {
		setTitle("Xử lý chuỗi String Builder");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 608);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Chuỗi sb");
		lblNewLabel.setBounds(6, 17, 132, 16);
		contentPane.add(lblNewLabel);
		
		txtSb = new JTextField();
		txtSb.setBounds(152, 12, 386, 26);
		contentPane.add(txtSb);
		txtSb.setColumns(10);
		
		JLabel lblChuiSb = new JLabel("Chuỗi sb1");
		lblChuiSb.setBounds(6, 50, 132, 16);
		contentPane.add(lblChuiSb);
		
		txtSb1 = new JTextField();
		txtSb1.setColumns(10);
		txtSb1.setBounds(152, 45, 386, 26);
		contentPane.add(txtSb1);
		
		JLabel lblChuiSb_1 = new JLabel("Chuỗi sb2");
		lblChuiSb_1.setBounds(6, 83, 132, 16);
		contentPane.add(lblChuiSb_1);
		
		txtSb2 = new JTextField();
		txtSb2.setColumns(10);
		txtSb2.setBounds(152, 78, 386, 26);
		contentPane.add(txtSb2);
		
		JLabel lblVTrChn = new JLabel("Vị trí chèn");
		lblVTrChn.setBounds(6, 116, 132, 16);
		contentPane.add(lblVTrChn);
		
		txtInsert = new JTextField();
		txtInsert.setColumns(10);
		txtInsert.setBounds(152, 111, 386, 26);
		contentPane.add(txtInsert);
		
		JLabel lblVTru = new JLabel("Vị trí đầu");
		lblVTru.setBounds(6, 150, 132, 16);
		contentPane.add(lblVTru);
		
		txtFirst = new JTextField();
		txtFirst.setColumns(10);
		txtFirst.setBounds(152, 145, 386, 26);
		contentPane.add(txtFirst);
		
		JLabel lblVTrCui = new JLabel("Vị trí cuối");
		lblVTrCui.setBounds(6, 183, 132, 16);
		contentPane.add(lblVTrCui);
		
		txtEnd = new JTextField();
		txtEnd.setColumns(10);
		txtEnd.setBounds(152, 178, 386, 26);
		contentPane.add(txtEnd);
		
		JButton btnXLChui = new JButton("Xử lý chuỗi");
		btnXLChui.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StringBuilder sbResult = new StringBuilder("Kết quả: \n");
				StringBuilder sb = new StringBuilder();
				
				sb.append(txtSb.getText().trim());
				sbResult.append("Chuỗi sb: " + sb.toString() + "\n");
				sbResult.append("Chiều dài: " + sb.length() + "\n");
				
				sb.append(txtSb1.getText());
				sbResult.append("Chuỗi sau khi nối chuỗi sb1: " + sb.toString() + "\n");
				sbResult.append("Chiều dài: " + sb.length() + "\n");
				
				// Chuyển đổi kiểu chuỗi sang số nguyên -> Integer.parseInt(chuỗi)
				int viTri = Integer.parseInt(txtInsert.getText());
				sb.insert(viTri, txtSb2.getText().trim());
				sbResult.append("Sau khi chèn sb2 vào vịt trí " + viTri + ": " + sb.toString() + "\n");
				
				sb.delete(Integer.parseInt(txtFirst.getText().trim()), Integer.parseInt(txtEnd.getText().trim()));
				sbResult.append("Sau khi xóa " + sb.toString() + "\n");
				
				sb.reverse();
				sbResult.append("Sau khi đảo ngược " + sb.toString() + "\n");
				
				// Chuyển sbResult hiển thị lên GUI: TextARea
				taResult.setText(sbResult.toString());
				
			}
		});
		btnXLChui.setBounds(163, 234, 168, 45);
		contentPane.add(btnXLChui);
		
		JLabel label_6 = new JLabel("New label");
		label_6.setBounds(6, 309, 132, 16);
		contentPane.add(label_6);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(147, 309, 391, 255);
		contentPane.add(scrollPane);
		
		taResult = new JTextArea();
		scrollPane.setViewportView(taResult);
	}
}
