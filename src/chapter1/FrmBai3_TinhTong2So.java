package chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_TinhTong2So extends JFrame {

	private JPanel contentPane;
	private JTextField txtSoNguyen1;
	private JTextField txtSoNguyen2;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_TinhTong2So frame = new FrmBai3_TinhTong2So();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_TinhTong2So() {
		setTitle("Tính tổng 2 số");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 225);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Số nguyên thứ nhất");
		lblNewLabel.setBounds(17, 17, 137, 16);
		contentPane.add(lblNewLabel);
		
		txtSoNguyen1 = new JTextField();
		txtSoNguyen1.setBounds(195, 10, 249, 31);
		contentPane.add(txtSoNguyen1);
		txtSoNguyen1.setColumns(10);
		
		txtSoNguyen2 = new JTextField();
		txtSoNguyen2.setColumns(10);
		txtSoNguyen2.setBounds(195, 45, 249, 31);
		contentPane.add(txtSoNguyen2);
		
		JLabel lblSNguynTh = new JLabel("Số nguyên thứ 2");
		lblSNguynTh.setBounds(17, 52, 113, 16);
		contentPane.add(lblSNguynTh);
		
		txtKetQua = new JTextField();
		txtKetQua.setEditable(false);
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(195, 88, 249, 31);
		contentPane.add(txtKetQua);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(17, 95, 113, 16);
		contentPane.add(lblKtQu);
		
		JButton btnTnhTng = new JButton("Tính Tổng");
		btnTnhTng.addActionListener(new ActionListener() {
			/**
			 * Giải thuật tính tổng 2 số nguyên
			 */
			public void actionPerformed(ActionEvent e) {
				// B1. Lấy dữ liệu người dùng nhập vào
				String sSoNguyen1 = txtSoNguyen1.getText();
				String sSoNguyen2 = txtSoNguyen2.getText();
				
				// B2. Tính tổng 2 số -> Lưu ý: chuyển chuổi -> số nguyên => Integer.parseInt
				int soNguyen1 = Integer.parseInt(sSoNguyen1);
				int soNguyen2 = Integer.parseInt(sSoNguyen2);
				
				int ketQua = soNguyen1 + soNguyen2;
				
				// B3. Hiện thị kết quả => Lưu ý: chuyển số nguyên -> chuỗi
				txtKetQua.setText(String.valueOf(ketQua));
			}
		});
		btnTnhTng.setBounds(140, 143, 117, 40);
		contentPane.add(btnTnhTng);
	}
}
