package chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_HelloWorld extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_HelloWorld frame = new FrmBai2_HelloWorld();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_HelloWorld() {
		setTitle("Xin chào");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 458, 140);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpHTn = new JLabel("Nhập họ tên:");
		lblNhpHTn.setBounds(60, 26, 70, 14);
		contentPane.add(lblNhpHTn);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(140, 23, 269, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lblHienThi = new JLabel("");
		lblHienThi.setBounds(10, 51, 430, 14);
		contentPane.add(lblHienThi);
		
		JButton btnXutLiCho = new JButton("Xuất lời chào");
		btnXutLiCho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Lay ho ten nguoi dung nhap
				String hoTen = txtHoTen.getText();
				
				// Xuat ra loi chao
				lblHienThi.setText("Chào bạn " + hoTen + ", chào mừng bạn đến với ngôn ngữ lập trình java.");
			}
		});
		btnXutLiCho.setBounds(160, 76, 121, 23);
		contentPane.add(btnXutLiCho);
	}
}
