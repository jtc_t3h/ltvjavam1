package chapter3;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

public class MyMouseListener2 extends MouseAdapter {

	@Override
	public void mouseEntered(MouseEvent e) {
		JOptionPane.showMessageDialog(null, "Mouse enter here.");
	}
	
}
