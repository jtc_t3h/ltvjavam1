package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.event.InputEvent;
import javax.swing.JSeparator;

public class FrmDemo1 extends JFrame{

	private JPanel contentPane;
	private JTextField txtBrowser;
	private ButtonGroup genderGroup = new ButtonGroup();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo1 frame = new FrmDemo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 557);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);
		
		JMenu mnNew = new JMenu("New");
		mnFile.add(mnNew);
		
		JMenuItem mntmJavaProject = new JMenuItem("Java Project");
		mnNew.add(mntmJavaProject);
		
		JMenuItem mntmProject = new JMenuItem("Project ...");
		mnNew.add(mntmProject);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmOpen = new JMenuItem("Open ...");
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.ALT_MASK));
		mnFile.add(mntmOpen);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnOpenDialog = new JButton("Open Dialog");
		btnOpenDialog.setBounds(5, 28, 424, 23);
		btnOpenDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Gọi và hiển thị dialog từ 1 frame: tạo đối tượng -> setVisible = true
				DlgDemo1 dlg = new DlgDemo1();
				dlg.setLocationRelativeTo(contentPane);
				dlg.setModal(true);
				dlg.setVisible(true);
			}
		});
		contentPane.setLayout(null);
		contentPane.add(btnOpenDialog);
		
		JPanel panel = new JPanel();
		panel.setToolTipText("Click here to open Dialog Demo");
		panel.setBorder(new TitledBorder(null, "Search panel", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(5, 62, 424, 90);
		contentPane.add(panel);
		
		txtBrowser = new JTextField();
		txtBrowser.setBounds(5, 163, 309, 20);
		contentPane.add(txtBrowser);
		txtBrowser.setColumns(10);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcs = new JFileChooser();
				fcs.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				if (fcs.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					// Đã chọn file hoặc directory
					java.io.File fileSelected = fcs.getSelectedFile();
					txtBrowser.setText(fileSelected.getAbsolutePath());
				}
			}
		});
		btnBrowser.setBounds(324, 163, 89, 23);
		contentPane.add(btnBrowser);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBounds(6, 215, 109, 23);
		contentPane.add(rdbtnNam);
		genderGroup.add(rdbtnNam);
		
		JRadioButton rdbtnNu = new JRadioButton("Nu");
		rdbtnNu.setBounds(131, 215, 109, 23);
		contentPane.add(rdbtnNu);
		genderGroup.add(rdbtnNu);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 261, 187, 247);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton btnEvent = new JButton("Event");
		btnEvent.addMouseListener(new MyMouseListener());
		btnEvent.setBounds(260, 215, 89, 23);
		contentPane.add(btnEvent);
	}
}
