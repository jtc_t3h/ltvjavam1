package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utils.ImageRender;
import utils.ReadOrWriteFile;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

public class FrmBai6_BookTicket extends JFrame {

	private JPanel contentPane;
	private JTextField txtSeats;
	private JTextField txtName;
	private JComboBox cbbMovies;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai6_BookTicket frame = new FrmBai6_BookTicket();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai6_BookTicket() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 398, 273);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSeats = new JLabel("Seat(s)");
		lblSeats.setBounds(10, 11, 46, 14);
		contentPane.add(lblSeats);
		
		txtSeats = new JTextField();
		txtSeats.setBounds(86, 8, 101, 20);
		contentPane.add(txtSeats);
		txtSeats.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 36, 46, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(86, 33, 286, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblMovies = new JLabel("Movies");
		lblMovies.setBounds(10, 90, 46, 14);
		contentPane.add(lblMovies);
		
		// B1: Lay danh sach Film tu file
		String[] data = getListFilm();
		Integer[] idx = new Integer[data.length];
		for (int i = 0; i < data.length; i++) {
			idx[i] = i;
		}
		
		cbbMovies = new JComboBox(idx);
		cbbMovies.setRenderer(new ImageRender(data));
//		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Big Hero", "Avatar"}));
		cbbMovies.setBounds(86, 64, 195, 70);
		contentPane.add(cbbMovies);
		
		JButton btnBook = new JButton("Book");
		btnBook.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String seats = txtSeats.getText().trim();
				String name = txtName.getText().trim();
				String movies = data[cbbMovies.getSelectedIndex()];
				
				// Truyen du lieu qua Dialog
				DlgBai7_MessageBox dlg = new DlgBai7_MessageBox(seats, name, movies);
				dlg.setVisible(true);
			}
		});
		btnBook.setBounds(132, 201, 89, 23);
		contentPane.add(btnBook);
		
//		initModelOfCombobox();
		
		
	}

	private String[] getListFilm() {
		File file = new File("src/FilmList.txt");
		try {
			String content = ReadOrWriteFile.readTXT(file);
			
			return content.split("\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void initModelOfCombobox() {
		// B1. Tao ra mang doi tuong (String, Department,...)
		String[] arr = {"Big Hero", "Avatar"};
		
		// B2. Tao doi tuong thuoc DefaultComboboxModel
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>(arr);
		
		// B3. thiet lap thuoc tinh model
		cbbMovies.setModel(model);
	}
}
