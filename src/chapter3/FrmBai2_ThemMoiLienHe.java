package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class FrmBai2_ThemMoiLienHe extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDTDD;
	private JTextField txtHinhAnh;
	private JLabel lblHinhAnh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_ThemMoiLienHe frame = new FrmBai2_ThemMoiLienHe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_ThemMoiLienHe() {
		setTitle("Thêm mới liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 561, 222);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ tên");
		lblNewLabel.setBounds(10, 21, 75, 14);
		contentPane.add(lblNewLabel);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(95, 18, 274, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lbltd = new JLabel("ĐTDĐ");
		lbltd.setBounds(10, 67, 75, 14);
		contentPane.add(lbltd);
		
		txtDTDD = new JTextField();
		txtDTDD.setColumns(10);
		txtDTDD.setBounds(95, 64, 147, 20);
		contentPane.add(txtDTDD);
		
		JLabel lblHnhnh = new JLabel("Hình ảnh");
		lblHnhnh.setBounds(10, 110, 75, 14);
		contentPane.add(lblHnhnh);
		
		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(95, 107, 216, 20);
		contentPane.add(txtHinhAnh);
		
		JButton btnNewButton = new JButton("...");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Hiển thị JFileChooser để chọn hình
				JFileChooser fcs = new JFileChooser();
				fcs.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				
				if (fcs.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File fileSelected = fcs.getSelectedFile();
					
					txtHinhAnh.setText(fileSelected.getAbsolutePath());
					
					ImageIcon icon = new ImageIcon(fileSelected.getAbsolutePath());
					lblHinhAnh.setIcon(icon);
					lblHinhAnh.setBounds(392, 11, 134, 163);
				};
			}
		});
		btnNewButton.setBounds(321, 106, 48, 23);
		contentPane.add(btnNewButton);
		
		JButton btnThmMi = new JButton("Thêm mới");
		btnThmMi.setBounds(95, 151, 111, 23);
		contentPane.add(btnThmMi);
		
		lblHinhAnh = new JLabel("");
		contentPane.add(lblHinhAnh);
	}

}
