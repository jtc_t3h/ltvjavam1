package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai1_ThongTinLienHe extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDTDD;
	private JTextField txtHinhAnh;
	private JLabel lblHoTen;
	private JLabel lblDTDD;
	private JLabel lblHinhAnh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_ThongTinLienHe frame = new FrmBai1_ThongTinLienHe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_ThongTinLienHe() {
		setTitle("Thông tin liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 653);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ tên");
		lblNewLabel.setBounds(18, 22, 137, 16);
		contentPane.add(lblNewLabel);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(163, 17, 334, 26);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lbltd = new JLabel("ĐTDĐ");
		lbltd.setBounds(18, 60, 127, 16);
		contentPane.add(lbltd);
		
		txtDTDD = new JTextField();
		txtDTDD.setColumns(10);
		txtDTDD.setBounds(163, 55, 334, 26);
		contentPane.add(txtDTDD);
		
		JLabel lblHnh = new JLabel("Hình");
		lblHnh.setBounds(18, 98, 137, 16);
		contentPane.add(lblHnh);
		
		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(163, 93, 334, 26);
		contentPane.add(txtHinhAnh);
		
		JButton btnHinTh = new JButton("Hiển thị");
		btnHinTh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				lblHoTen.setText(txtHoTen.getText().trim());
				lblDTDD.setText(txtDTDD.getText().trim());
				
				// Hien thi hinh anh
				// B1. Tao doi tuong ImageIcon
				ImageIcon icon = new ImageIcon(txtHinhAnh.getText().trim());
				lblHinhAnh.setIcon(icon);
				
			}
		});
		btnHinTh.setBounds(142, 147, 137, 29);
		contentPane.add(btnHinTh);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(18, 200, 467, 6);
		contentPane.add(separator);
		
		JLabel label = new JLabel("Họ tên");
		label.setBounds(18, 241, 137, 16);
		contentPane.add(label);
		
		lblHoTen = new JLabel("");
		lblHoTen.setBounds(167, 241, 318, 16);
		contentPane.add(lblHoTen);
		
		JLabel lbltd_1 = new JLabel("ĐTDĐ");
		lbltd_1.setBounds(18, 281, 137, 16);
		contentPane.add(lbltd_1);
		
		lblDTDD = new JLabel("");
		lblDTDD.setBounds(167, 281, 318, 16);
		contentPane.add(lblDTDD);
		
		JLabel lblHnhnh = new JLabel("Hình ảnh");
		lblHnhnh.setBounds(18, 320, 137, 16);
		contentPane.add(lblHnhnh);
		
		lblHinhAnh = new JLabel("");
		lblHinhAnh.setIcon(new ImageIcon("/Users/phaolo/Downloads/hinh-anh-chan-dung-blend-and-retouch-dep1.jpg"));
		lblHinhAnh.setBounds(163, 320, 216, 292);
		contentPane.add(lblHinhAnh);
	}
}
