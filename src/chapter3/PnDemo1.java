package chapter3;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PnDemo1 extends JPanel {
	private JRadioButton rdbDemo;

	// Instance = Field
	
	/**
	 * Create the panel.
	 */
	public PnDemo1() {
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(23, 11, 46, 14);
		add(lblNewLabel);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		chckbxNewCheckBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}
		});
		chckbxNewCheckBox.setBounds(144, 7, 97, 23);
		add(chckbxNewCheckBox);
		
		
		// local
		rdbDemo = new JRadioButton("New radio button");
		rdbDemo.setBounds(23, 59, 109, 23);
		add(rdbDemo);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(156, 60, 260, 20);
		add(comboBox);

	}
}
