package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;

public class FrmBai4_MenuBar extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_MenuBar frame = new FrmBai4_MenuBar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_MenuBar() {
		setTitle("Hệ thống bài tập mô đun 1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnBi = new JMenu("Bài 1");
		mnBi.setMnemonic('1');
		menuBar.add(mnBi);
		
		JMenu mnBai = new JMenu("Bài 2");
		mnBai.setMnemonic('2');
		menuBar.add(mnBai);
		
		JMenu mnBi_1 = new JMenu("Bài 3");
		mnBi_1.setMnemonic('3');
		menuBar.add(mnBi_1);
		
		JMenuItem mntmBi = new JMenuItem("Bài 3.1");
		mntmBi.setIcon(new ImageIcon(FrmBai4_MenuBar.class.getResource("/images/Cart-icon.png")));
		mntmBi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		mntmBi.setEnabled(false);
		mnBi_1.add(mntmBi);
		
		JSeparator separator = new JSeparator();
		mnBi_1.add(separator);
		
		JMenu mnNewMenu = new JMenu("New menu");
		mnBi_1.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
}
