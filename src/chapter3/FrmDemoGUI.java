package chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JRadioButton;

public class FrmDemoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtFileChooser;
	private JComboBox comboBox;
	
	private ButtonGroup genderGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemoGUI frame = new FrmDemoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 602);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Demo JFileChooser", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(6, 6, 858, 69);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblChooseFile = new JLabel("Choose file");
		lblChooseFile.setBounds(16, 31, 84, 16);
		panel.add(lblChooseFile);
		
		txtFileChooser = new JTextField();
//		txtFileChooser.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseEntered(MouseEvent e) {
//				JOptionPane.showMessageDialog(null, "mouser enter here.");
//			}
//		});
		txtFileChooser.addMouseListener(new MyMouseListener2());
		
		
		txtFileChooser.setEditable(false);
		txtFileChooser.setBounds(110, 26, 486, 26);
		panel.add(txtFileChooser);
		txtFileChooser.setColumns(10);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				JOptionPane.showMessageDialog(contentPane, "Mouse enter event");
			}
		});
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcs = new JFileChooser();
				
				fcs.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if (fcs.showOpenDialog(contentPane) == JFileChooser.APPROVE_OPTION) {
				
					txtFileChooser.setText(fcs.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnBrowser.setBounds(616, 26, 117, 29);
		panel.add(btnBrowser);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"--- Giới tính ---", "Nam", "Nữ", "Undefine"}));
		comboBox.setBounds(16, 90, 193, 27);
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(220, 90, 291, 27);
		contentPane.add(comboBox_1);
		
		// B1: Tạo mảng tĩnh
		String[] listOfDepartent = new String[] {"--- Phòng ban ----", "P. CNTT", "P.CSKH"};
		// B2: Tạo đối DefaultComboboxModel
		DefaultComboBoxModel model = new DefaultComboBoxModel(listOfDepartent);
		// B3: thiết lập thuộc tính model
		comboBox_1.setModel(model);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newButtonActionPerformed(e);
			}
		});
		btnNewButton.setBounds(10, 207, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnOpenDialog = new JButton("Open Dialog");
		btnOpenDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DlgDemo dlg = new DlgDemo();
				dlg.setModal(true);
				dlg.setVisible(true);
			}
		});
		btnOpenDialog.setBounds(719, 89, 117, 29);
		contentPane.add(btnOpenDialog);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBounds(16, 153, 141, 23);
		contentPane.add(rdbtnNam);
		genderGroup.add(rdbtnNam);
		
		JRadioButton rdbtnNu = new JRadioButton("Nu");
		rdbtnNu.setBounds(183, 153, 141, 23);
		contentPane.add(rdbtnNu);
		genderGroup.add(rdbtnNu);
		
		JButton btnEventlistener = new JButton("Event-Listener");
		btnEventlistener.setBounds(425, 169, 117, 29);
		contentPane.add(btnEventlistener);
		btnEventlistener.addActionListener(new MyActionListener());
	}

	protected void newButtonActionPerformed(ActionEvent e) {
		
	}
	
	
}
