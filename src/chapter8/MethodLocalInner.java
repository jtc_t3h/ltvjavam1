package chapter8;

public class MethodLocalInner {

	public void aMethod() {
		
		// Định nghĩa lớp lồng cấp cục bộ hàm
		class MyInner {
			public void print() {
				System.out.println("Method local inner class.");
			}
		}
		
		// Sử dụng lớp lồng cấp cục bộ hàm 
		// 1. Tạo đối tượng
		MyInner inner = new MyInner();
		// 2. Gọi hàm
		inner.print();
	}

}
