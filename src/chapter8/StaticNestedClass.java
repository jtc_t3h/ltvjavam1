package chapter8;

public class StaticNestedClass {

	static class MyInner {
		
		public void print() {
			System.out.println("Static nested class.");
		}
	}
}
