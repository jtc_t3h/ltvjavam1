package chapter8;

import java.util.Arrays;
import java.util.Comparator;

public class AnnonymousInnerClass {
	
	public static void main(String[] args) {
		// 1. Định nghĩa lớp lồng vô danh, nhưng lớp này hiện thực giao diện List
		// 2. Tạo đối tượng của lớp vô danh 
		Comparator<String> comparator = new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1); // Sort theo thứ tự từ lớn -> nhỏ
			}
		};
		
		String[] arr = {"abc", "def", "abd"};
		
		// Bài toán sort trên mảng -> sort từ lớn đến nhỏ
		// Thực hiện so sánh 2 chuỗi -> lớn bé -> kết quả trả về < 0 (trước < sau), 0 (= nhau), > 0 (trước > sau)
		Arrays.sort(arr, comparator);
		
		// print để kiểm tra kết quả sort
		for (String e: arr) {
			System.out.println(e);
		}
	}
}
