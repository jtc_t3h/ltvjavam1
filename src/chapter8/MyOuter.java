package chapter8;

// Top-level class -> class Name = file Name
public class MyOuter {

	private String instanceVar;
	
	public void aMethod() {
		System.out.println("MyOuter: aMethod");
		
		// Không cần tạo đối tượng lớp MyOuter
		MyInner myInner = new MyInner();
		myInner.another();
	}
	
	// Regular inner class 
	// Ngang cấp với biến thực thể (instanceVar) hoặc phương thức (aMethod)
	// Modifier áp dụng cho biến thực thể và cho phương thức thì đều áp dụng được cho lớp lồng cấp chính quy
	// Access Modifier (public, protected, private, [default]) và others (final, abstract, [static])
	/* [Modifier] */ class MyInner {
		private String x;
		
		public void another() {
			System.out.println("MyInner: another.");
			System.out.println(instanceVar); // Special relationship
		}
	}
}
