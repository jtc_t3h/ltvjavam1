package chapter8;

import chapter8.MyOuter.MyInner;

public class TestClient {

	public static void main(String[] args) {
		
		// Tạo đối tượng lớp lồng cấp chính quy (regular inner class) ở bên ngoài lớp MyOuter
		// 1. Tạo đối tượng lớp MyOuter -> có 2 cách
		// 2. Tạo đối tượng lớp lồng cấp
		
		// Cách 1: Tạo đối tượng MyOuter với tên biến (tường minh)
		MyOuter myOuter1 = new MyOuter();
		MyInner myInner = myOuter1.new MyInner();
		myOuter1.aMethod();
		myInner.another();
		
		// Cách 2: Tạo đối tượng MyOuter ẩn danh (không có tên)
		MyOuter.MyInner myInner2 = new MyOuter().new MyInner();
		myInner2.another();
		
		// Tạo đối tượng Static nested class -> không cần tạo đối tượng lớp ngoài
		StaticNestedClass.MyInner staticNested = new StaticNestedClass.MyInner();
		staticNested.print();
	}

}
