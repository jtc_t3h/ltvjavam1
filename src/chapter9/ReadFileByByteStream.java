package chapter9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ReadFileByByteStream {

	public static void main(String[] args) throws IOException {
		
		// Tao ra doi tuong doc file
		InputStream in = new FileInputStream("src/chapter9/input.txt");
		
		// Thuc hien xu ly file
		int i;
		while( (i = in.read()) != -1) {
			System.out.println((char)i);
		}

		// Dong file
		in.close();
	}

}
