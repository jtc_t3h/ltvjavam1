package chapter9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFileByteStream {

	public static void main(String[] args) throws IOException {
		
		// Tạo đối tượng FileInputStream để đọc file
		FileInputStream in = new FileInputStream("./src/chapter9/input.txt");
		
		// Cách 1: Đọc dữ liệu từ file -> dùng hàm read()
//		int ch;
//		while ( (ch = in.read()) != -1) {
//			System.out.println((char)ch);
//		}
		
		// Cách 2: Đọc dữ liệu theo mảng
		byte[] b = new byte[3];
		while (in.read(b) != -1) {
			for (byte ch: b) {
				System.out.print((char)ch);
			}
			System.out.println();
		}
		
		in.close();
		System.out.println("Done.");
	}
}
