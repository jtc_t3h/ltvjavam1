package chapter9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

public class BufferDemo {

	public static void main(String[] args) throws IOException {

		// chuong trinh nhap du lieu tu ban phim -> system.in : thiet bi nhap chuan
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.print("Nhap ho ten: ");
		String hoTen = br.readLine();
		System.out.println("Xin chao: " + hoTen);
		
		
	}

}
