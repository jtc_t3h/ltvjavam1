package chapter9;

import java.io.FileInputStream;

public class ReadFileByInputStream {

	public static void main(String[] args) {
		try (FileInputStream in = new FileInputStream("src/chapter9/input.txt")){
			byte []b = new byte[3];
			
			while(in.read(b) != -1) {
				for (byte e: b)	{
					System.out.print((char)e);
				}
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
