package chapter9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ReadFileByByteStream2 {

	public static void main(String[] args) {
		
		// Tao ra doi tuong doc file
		InputStream in = null;
		try {
			in = new FileInputStream("src/chapter9/input.txt");
			
			// Thuc hien xu ly file
			byte b[] = new byte[3];
			while(in.read(b) != -1) {
				for (byte e: b) {
					System.out.print((char)e);
				}
				System.out.println();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			// Dong file
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
	}

}
