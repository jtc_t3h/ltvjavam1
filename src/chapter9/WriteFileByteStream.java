package chapter9;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteFileByteStream {

	public static void main(String[] args) {
		String s = "hello world";
		
		try (FileOutputStream out = new FileOutputStream("./src/chapter9/out.txt")){
			for (byte ch: s.getBytes()) {
				out.write(ch);
			}
			out.flush();
			System.out.println("Done.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
